-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: guia
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cita`
--

DROP TABLE IF EXISTS `cita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cita` (
  `IdCita` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombrePaciente` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Fecha` date DEFAULT NULL,
  `Hora` time DEFAULT NULL,
  `fkIdDoctor` bigint(20) DEFAULT NULL,
  `fkIdEstado` bigint(20) DEFAULT NULL,
  `fkIdFactura` bigint(20) DEFAULT NULL,
  `fkIdTipoCita` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdCita`),
  KEY `CitaDoctor` (`fkIdDoctor`),
  KEY `CitaEstado` (`fkIdEstado`),
  KEY `CitaFactura` (`fkIdFactura`),
  KEY `CitaTipo` (`fkIdTipoCita`),
  CONSTRAINT `cita_ibfk_1` FOREIGN KEY (`fkIdDoctor`) REFERENCES `doctor` (`IdDoctor`),
  CONSTRAINT `cita_ibfk_2` FOREIGN KEY (`fkIdEstado`) REFERENCES `estado` (`IdEstado`),
  CONSTRAINT `cita_ibfk_3` FOREIGN KEY (`fkIdFactura`) REFERENCES `factura` (`IdFactura`),
  CONSTRAINT `cita_ibfk_4` FOREIGN KEY (`fkIdTipoCita`) REFERENCES `tipocita` (`IdTipoCita`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cita`
--

LOCK TABLES `cita` WRITE;
/*!40000 ALTER TABLE `cita` DISABLE KEYS */;
INSERT INTO `cita` VALUES (1,'Brayan','2016-05-10','06:08:12',2,1,2,1),(2,'Jason','2016-05-18','05:04:13',1,2,1,2);
/*!40000 ALTER TABLE `cita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `cita2`
--

DROP TABLE IF EXISTS `cita2`;
/*!50001 DROP VIEW IF EXISTS `cita2`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `cita2` (
  `IdCita` tinyint NOT NULL,
  `Fecha` tinyint NOT NULL,
  `Paciente` tinyint NOT NULL,
  `Hora` tinyint NOT NULL,
  `Tipo_Cita` tinyint NOT NULL,
  `Nombre_Doctor` tinyint NOT NULL,
  `EPS` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `citapaciente`
--

DROP TABLE IF EXISTS `citapaciente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `citapaciente` (
  `IdCitaPaciente` bigint(20) NOT NULL AUTO_INCREMENT,
  `Correo` varchar(100) DEFAULT NULL,
  `fkIdPaciente` bigint(20) DEFAULT NULL,
  `fkIdCita` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdCitaPaciente`),
  KEY `citapaciente` (`fkIdPaciente`),
  KEY `pacientecita` (`fkIdCita`),
  CONSTRAINT `citapaciente_ibfk_1` FOREIGN KEY (`fkIdPaciente`) REFERENCES `paciente` (`IdPaciente`),
  CONSTRAINT `citapaciente_ibfk_2` FOREIGN KEY (`fkIdCita`) REFERENCES `cita` (`IdCita`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `citapaciente`
--

LOCK TABLES `citapaciente` WRITE;
/*!40000 ALTER TABLE `citapaciente` DISABLE KEYS */;
INSERT INTO `citapaciente` VALUES (1,'fgfdg',1,2),(2,'fdg',2,1);
/*!40000 ALTER TABLE `citapaciente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diagnostico`
--

DROP TABLE IF EXISTS `diagnostico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diagnostico` (
  `IdDiagnostico` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(500) DEFAULT NULL,
  `fkIdPaciente` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdDiagnostico`),
  KEY `diagnosticoPaciente` (`fkIdPaciente`),
  CONSTRAINT `diagnostico_ibfk_1` FOREIGN KEY (`fkIdPaciente`) REFERENCES `paciente` (`IdPaciente`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diagnostico`
--

LOCK TABLES `diagnostico` WRITE;
/*!40000 ALTER TABLE `diagnostico` DISABLE KEYS */;
INSERT INTO `diagnostico` VALUES (1,'Tiene gripa',2),(2,'Tiene ojos',1);
/*!40000 ALTER TABLE `diagnostico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctor` (
  `IdDoctor` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `Identificacion` bigint(20) DEFAULT NULL,
  `TelefonoCelular` bigint(20) DEFAULT NULL,
  `TelefonoFijo` bigint(20) DEFAULT NULL,
  `Direccion` varchar(100) DEFAULT NULL,
  `fkIdEstado` bigint(20) DEFAULT NULL,
  `fkIdLogin` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdDoctor`),
  KEY `DoctorEstado` (`fkIdEstado`),
  KEY `DoctorLogin` (`fkIdLogin`),
  CONSTRAINT `doctor_ibfk_1` FOREIGN KEY (`fkIdEstado`) REFERENCES `estado` (`IdEstado`),
  CONSTRAINT `doctor_ibfk_2` FOREIGN KEY (`fkIdLogin`) REFERENCES `login` (`IdLogin`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` VALUES (1,'Nicol',123456,7897,987,'d879',1,2),(2,'Jaime',987,874,645,'f654',2,1);
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctorpaciente`
--

DROP TABLE IF EXISTS `doctorpaciente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctorpaciente` (
  `IdDoctorPaciente` bigint(20) NOT NULL AUTO_INCREMENT,
  `Correo` varchar(100) DEFAULT NULL,
  `fkIdPaciente` bigint(20) DEFAULT NULL,
  `fkIdDoctor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdDoctorPaciente`),
  KEY `pacientedoctor` (`fkIdPaciente`),
  KEY `doctorpaciente` (`fkIdDoctor`),
  CONSTRAINT `doctorpaciente_ibfk_1` FOREIGN KEY (`fkIdPaciente`) REFERENCES `paciente` (`IdPaciente`),
  CONSTRAINT `doctorpaciente_ibfk_2` FOREIGN KEY (`fkIdDoctor`) REFERENCES `doctor` (`IdDoctor`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctorpaciente`
--

LOCK TABLES `doctorpaciente` WRITE;
/*!40000 ALTER TABLE `doctorpaciente` DISABLE KEYS */;
INSERT INTO `doctorpaciente` VALUES (1,'jfgjas',1,2),(2,'sdasd',2,1);
/*!40000 ALTER TABLE `doctorpaciente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `IdEmpleado` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `Identificacion` bigint(20) DEFAULT NULL,
  `TelefonoCelular` bigint(20) DEFAULT NULL,
  `TelefonoFijo` bigint(20) DEFAULT NULL,
  `Direccion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IdEmpleado`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES (1,'Jose',123456789,3134600625,7137461,'av 12 n 12- 23 '),(2,'Maria',987456321,3123213712,466156,'dha3');
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eps`
--

DROP TABLE IF EXISTS `eps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eps` (
  `IdEPS` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `fkIdCita` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdEPS`),
  KEY `CitaEps` (`fkIdCita`),
  CONSTRAINT `eps_ibfk_1` FOREIGN KEY (`fkIdCita`) REFERENCES `cita` (`IdCita`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eps`
--

LOCK TABLES `eps` WRITE;
/*!40000 ALTER TABLE `eps` DISABLE KEYS */;
INSERT INTO `eps` VALUES (1,'Cafam',1),(2,'Colsubsidio',2);
/*!40000 ALTER TABLE `eps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `IdEstado` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IdEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` VALUES (1,'Activo'),(2,'Inactivo');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura`
--

DROP TABLE IF EXISTS `factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factura` (
  `IdFactura` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombrePaciente` varchar(100) DEFAULT NULL,
  `Valor` bigint(20) DEFAULT NULL,
  `Cantidad` bigint(20) DEFAULT NULL,
  `TelefonoCelular` bigint(20) DEFAULT NULL,
  `TelefonoFijo` bigint(20) DEFAULT NULL,
  `fkIdPaciente` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdFactura`),
  KEY `facturaPaciente` (`fkIdPaciente`),
  CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`fkIdPaciente`) REFERENCES `paciente` (`IdPaciente`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura`
--

LOCK TABLES `factura` WRITE;
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
INSERT INTO `factura` VALUES (1,'Maria',2000,1,64,654,1),(2,'Karen',3000,1,564,654,2);
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `historial`
--

DROP TABLE IF EXISTS `historial`;
/*!50001 DROP VIEW IF EXISTS `historial`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `historial` (
  `Identificacion_Paciente` tinyint NOT NULL,
  `Nombre_Paciente` tinyint NOT NULL,
  `Descripcion` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `historialmedico`
--

DROP TABLE IF EXISTS `historialmedico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historialmedico` (
  `IdHistorialMedico` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkIdPaciente` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdHistorialMedico`),
  KEY `historialpaciente` (`fkIdPaciente`),
  CONSTRAINT `historialmedico_ibfk_1` FOREIGN KEY (`fkIdPaciente`) REFERENCES `paciente` (`IdPaciente`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historialmedico`
--

LOCK TABLES `historialmedico` WRITE;
/*!40000 ALTER TABLE `historialmedico` DISABLE KEYS */;
INSERT INTO `historialmedico` VALUES (2,1),(4,1);
/*!40000 ALTER TABLE `historialmedico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventariomedicamento`
--

DROP TABLE IF EXISTS `inventariomedicamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventariomedicamento` (
  `IdInventarioMedicamento` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreMedicamento` varchar(100) DEFAULT NULL,
  `Existencias` bigint(20) DEFAULT NULL,
  `ExistenciasSalida` bigint(20) NOT NULL,
  `ExistenciasEntrada` bigint(20) NOT NULL,
  `fkIdMedicamento` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdInventarioMedicamento`),
  KEY `InventarioMedicamento` (`fkIdMedicamento`),
  CONSTRAINT `inventariomedicamento_ibfk_1` FOREIGN KEY (`fkIdMedicamento`) REFERENCES `medicamento` (`IdMedicamento`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventariomedicamento`
--

LOCK TABLES `inventariomedicamento` WRITE;
/*!40000 ALTER TABLE `inventariomedicamento` DISABLE KEYS */;
INSERT INTO `inventariomedicamento` VALUES (1,'acetaminofen',1000,200,300,1),(2,'Dolex',200,100,400,2);
/*!40000 ALTER TABLE `inventariomedicamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `IdLogin` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `Usuario` varchar(100) DEFAULT NULL,
  `Contraseña` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IdLogin`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES (1,'Jason','Jason123','123456'),(2,'Brayan','Brayan123','Rojas123');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicamento`
--

DROP TABLE IF EXISTS `medicamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicamento` (
  `IdMedicamento` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `Valor` bigint(20) DEFAULT NULL,
  `fkIdTipoMedicamento` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdMedicamento`),
  KEY `MedicamentoTipo` (`fkIdTipoMedicamento`),
  CONSTRAINT `medicamento_ibfk_1` FOREIGN KEY (`fkIdTipoMedicamento`) REFERENCES `tipomedicamento` (`IdTipoMedicamento`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicamento`
--

LOCK TABLES `medicamento` WRITE;
/*!40000 ALTER TABLE `medicamento` DISABLE KEYS */;
INSERT INTO `medicamento` VALUES (1,'Acetaminofen',2000,1),(2,'Dolex',3000,2);
/*!40000 ALTER TABLE `medicamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paciente`
--

DROP TABLE IF EXISTS `paciente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paciente` (
  `IdPaciente` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `Identificacion` bigint(20) DEFAULT NULL,
  `TelefonoCelular` bigint(20) DEFAULT NULL,
  `TelefonoFijo` bigint(20) DEFAULT NULL,
  `Direccion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IdPaciente`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paciente`
--

LOCK TABLES `paciente` WRITE;
/*!40000 ALTER TABLE `paciente` DISABLE KEYS */;
INSERT INTO `paciente` VALUES (1,'Gabriel',23165,654,654654,'4685'),(2,'Maria',64654,54654,65465,'f4654');
/*!40000 ALTER TABLE `paciente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `ror`
--

DROP TABLE IF EXISTS `ror`;
/*!50001 DROP VIEW IF EXISTS `ror`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ror` (
  `IdMedicamento_M` tinyint NOT NULL,
  `Medicamento_M` tinyint NOT NULL,
  `Existencias_M` tinyint NOT NULL,
  `Existencias_Entrada` tinyint NOT NULL,
  `Existencias_Salida` tinyint NOT NULL,
  `Valor_` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `tipocita`
--

DROP TABLE IF EXISTS `tipocita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipocita` (
  `IdTipoCita` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IdTipoCita`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipocita`
--

LOCK TABLES `tipocita` WRITE;
/*!40000 ALTER TABLE `tipocita` DISABLE KEYS */;
INSERT INTO `tipocita` VALUES (1,'Medicina General'),(2,'Pediatria');
/*!40000 ALTER TABLE `tipocita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipohistorial`
--

DROP TABLE IF EXISTS `tipohistorial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipohistorial` (
  `IdTipoHistorial` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `fkIdHistorialMedico` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdTipoHistorial`),
  KEY `TipoHistorial` (`fkIdHistorialMedico`),
  CONSTRAINT `tipohistorial_ibfk_1` FOREIGN KEY (`fkIdHistorialMedico`) REFERENCES `historialmedico` (`IdHistorialMedico`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipohistorial`
--

LOCK TABLES `tipohistorial` WRITE;
/*!40000 ALTER TABLE `tipohistorial` DISABLE KEYS */;
INSERT INTO `tipohistorial` VALUES (1,'MedicinaGeneral',4),(2,'Odontologia',2);
/*!40000 ALTER TABLE `tipohistorial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipomedicamento`
--

DROP TABLE IF EXISTS `tipomedicamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipomedicamento` (
  `IdTipoMedicamento` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IdTipoMedicamento`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipomedicamento`
--

LOCK TABLES `tipomedicamento` WRITE;
/*!40000 ALTER TABLE `tipomedicamento` DISABLE KEYS */;
INSERT INTO `tipomedicamento` VALUES (1,'Pastas'),(2,'Liquido');
/*!40000 ALTER TABLE `tipomedicamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `verdoctor`
--

DROP TABLE IF EXISTS `verdoctor`;
/*!50001 DROP VIEW IF EXISTS `verdoctor`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `verdoctor` (
  `IdDoctor_IdDoctorD` tinyint NOT NULL,
  `Nombre_NombreD` tinyint NOT NULL,
  `Nombre_EstadoP` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `verempleado`
--

DROP TABLE IF EXISTS `verempleado`;
/*!50001 DROP VIEW IF EXISTS `verempleado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `verempleado` (
  `IdEmpleado_IdEmpleadoP` tinyint NOT NULL,
  `Nombre_NombreP` tinyint NOT NULL,
  `Nombre_EstadoP` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `verpaciente`
--

DROP TABLE IF EXISTS `verpaciente`;
/*!50001 DROP VIEW IF EXISTS `verpaciente`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `verpaciente` (
  `IdPaciente_IdPacienteP` tinyint NOT NULL,
  `Nombre_NombreP` tinyint NOT NULL,
  `Nombre_Estado` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `cita2`
--

/*!50001 DROP TABLE IF EXISTS `cita2`*/;
/*!50001 DROP VIEW IF EXISTS `cita2`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `cita2` AS select `cita`.`IdCita` AS `IdCita`,`cita`.`Fecha` AS `Fecha`,`cita`.`NombrePaciente` AS `Paciente`,`cita`.`Hora` AS `Hora`,`tipocita`.`Nombre` AS `Tipo_Cita`,`doctor`.`Nombre` AS `Nombre_Doctor`,`eps`.`Nombre` AS `EPS` from (((`cita` join `tipocita`) join `doctor`) join `eps`) where (`eps`.`fkIdCita` = `cita`.`IdCita`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `historial`
--

/*!50001 DROP TABLE IF EXISTS `historial`*/;
/*!50001 DROP VIEW IF EXISTS `historial`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `historial` AS select `paciente`.`IdPaciente` AS `Identificacion_Paciente`,`paciente`.`Nombre` AS `Nombre_Paciente`,`diagnostico`.`Descripcion` AS `Descripcion` from (`paciente` join `diagnostico`) where (`diagnostico`.`fkIdPaciente` = `paciente`.`IdPaciente`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ror`
--

/*!50001 DROP TABLE IF EXISTS `ror`*/;
/*!50001 DROP VIEW IF EXISTS `ror`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ror` AS select `medicamento`.`IdMedicamento` AS `IdMedicamento_M`,`medicamento`.`Nombre` AS `Medicamento_M`,`inventariomedicamento`.`Existencias` AS `Existencias_M`,`inventariomedicamento`.`ExistenciasEntrada` AS `Existencias_Entrada`,`inventariomedicamento`.`ExistenciasSalida` AS `Existencias_Salida`,`medicamento`.`Valor` AS `Valor_` from (`medicamento` join `inventariomedicamento`) where (`inventariomedicamento`.`fkIdMedicamento` = `medicamento`.`IdMedicamento`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `verdoctor`
--

/*!50001 DROP TABLE IF EXISTS `verdoctor`*/;
/*!50001 DROP VIEW IF EXISTS `verdoctor`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `verdoctor` AS select `doctor`.`IdDoctor` AS `IdDoctor_IdDoctorD`,`doctor`.`Nombre` AS `Nombre_NombreD`,`estado`.`Nombre` AS `Nombre_EstadoP` from (`doctor` join `estado`) where (`estado`.`Nombre` = 'Activo') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `verempleado`
--

/*!50001 DROP TABLE IF EXISTS `verempleado`*/;
/*!50001 DROP VIEW IF EXISTS `verempleado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `verempleado` AS select `empleado`.`IdEmpleado` AS `IdEmpleado_IdEmpleadoP`,`empleado`.`Nombre` AS `Nombre_NombreP`,`estado`.`Nombre` AS `Nombre_EstadoP` from (`empleado` join `estado`) where (`estado`.`Nombre` = 'Activo') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `verpaciente`
--

/*!50001 DROP TABLE IF EXISTS `verpaciente`*/;
/*!50001 DROP VIEW IF EXISTS `verpaciente`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `verpaciente` AS select `paciente`.`IdPaciente` AS `IdPaciente_IdPacienteP`,`paciente`.`Nombre` AS `Nombre_NombreP`,`estado`.`Nombre` AS `Nombre_Estado` from (`paciente` join `estado`) where (`estado`.`Nombre` = 'Activo') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-19 20:08:18
